package utils;

public class Config {
  public static final int mainWIDTH = 360;
  public static final int logoHEIGHT = 210;
  public static final int mainHEIGHT = 420;
  public static final int gameWIDTH = 960;
  public static final int gameHEIGHT = 720;

  /* Change if you want to try different sizes of stones */
  public static final int stoneWIDTH = 95;

  /* Waiting time (ms) before simpleAI plays */
  public static final int WAIT = 500;
}
