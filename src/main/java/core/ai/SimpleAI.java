package core.ai;

import core.Game;
import core.Gamefield;
import gui.PaintingPanelSingle;
import utils.Config;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Implementation of simple artificial intelligence
 * It runs in its own thread
 */
public class SimpleAI{
  private PaintingPanelSingle paintingPanelSingle;
  private Game game;
  private Gamefield gamefield;
  private int[][] directions = {{0, 1}, {1, 1}, {1, 0}, {1, -1}, {0, -1}, {-1, -1}, {-1, 0}, {-1, 1}};
  /**
   * Constructor takes as a parameter paintingPanelSingle, because it needs to call repaint() method.
   * Other parameters: game, gamefield - for choosing the move
   *
   * @param paintingPanelSingle - PaintingPanelSingle object - needed to call repaint() method
   * @param game - Game object - needed for getting game status and variables
   * @param gamefield - Gamefield object - needed for stone position
   */
  public SimpleAI(PaintingPanelSingle paintingPanelSingle, Game game, Gamefield gamefield) {
    this.paintingPanelSingle = paintingPanelSingle;
    this.game = game;
    this.gamefield = gamefield;
  }

  /**
   * Checks if there is any stone near the move position
   * @param move - move[0] - column, move[1] - row
   * @return - Returns true if there is any stone near the move position, otherwise false
   */
  private boolean isStoneNear(int[] move) {
    int dx, dy, stone;
    for (int i = 0; i < 8; i++) {
      dx = directions[i][0];
      dy = directions[i][1];
      stone = gamefield.getItemGamefield(move[0] + dx, move[1] + dy);
      if (stone == 0 || stone == 1) {
        return true;
      }
    }
    return false;
  }

  /**
   * Chooses move and write it in the gamefield
   */
  public void play() {
    List<int[]> allMoves = new ArrayList<>();
    List<int[]> moves = new ArrayList<>();
    for (int y = 1; y < gamefield.getArraySize() - 1; y++) {
      for (int x = 1; x < gamefield.getArraySize() - 1; x++) {
        if (game.isValidMove(new int[]{x, y})) {
          allMoves.add(new int[]{x, y});
          if (isStoneNear(new int[]{x, y})) {
            moves.add(new int[]{x, y});
          }
        }
      }
    }
    if (moves.size() > 0) {
      game.writeMove(moves.get(new Random().nextInt(moves.size())));
    } else {
      game.writeMove(allMoves.get(new Random().nextInt(moves.size())));
    }
    paintingPanelSingle.repaint();
  }
}
