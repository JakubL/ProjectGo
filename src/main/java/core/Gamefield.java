package core;

/**
 * Gamefield implementation
 */
public class Gamefield {
  private int size;
  private int[][] gameArray;

  /**
   * Constructor takes size as parameter
   * It adds 2 to the size because the 2D array of the gamefiled is bounded by one more element
   * * @param size
   */
  public Gamefield(int size) {
    this.size = size + 2;
    this.gameArray = new int[this.size][this.size];
    this.fillArray();
  }

  /**
   * Method for initializing of empty gamefield
   */
  private void fillArray() {
    for (int y = 0; y < this.size; y++) {
      for (int x = 0; x < this.size; x++) {
        if ((x == 0) || (x == this.size - 1) || (y == 0) || (y == this.size - 1)) {
          gameArray[x][y] = 2;
        } else {
          gameArray[x][y] = -1;
        }
      }
    }
  }

  public int getItemGamefield(int x, int y) {
    return gameArray[x][y];
  }


  public void setItemGamefield(int x, int y, int stone) {
    gameArray[x][y] = stone;
  }

  /**
   * Method for getting the size of arrays in gamefield
   * @return size of arrays of gamefield (int)
   */
  public int getArraySize() {
    return size;
  }
}
