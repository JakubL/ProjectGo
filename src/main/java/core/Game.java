package core;

/**
 * Class for game implementation
 */
public class Game {
  private int playerIdx = 0;
  private int winner = 0;
  private int gameArraySize;
  private Gamefield gamefield;
  private GamefieldUtils gamefieldUtils;
  private boolean gameRunning = true;
  private boolean[] pass = new boolean[]{false, false};
  private float[] score = new float[]{0, 0.5f};

  /**
   * Constructor takes gamefield as parameter
   * It creates gamefieldUtils object
   *
   * @param gamefield -  Gamefield object
   */
  public Game(Gamefield gamefield) {
    this.gamefield = gamefield;
    gameArraySize = gamefield.getArraySize();
    gamefieldUtils = new GamefieldUtils(gamefield);
  }

  /**
   * Decides whether the move (coord) is valid
   *
   * @param coord - coord[0] is x coordinate, coord[1] is y coordinate
   * @return - Returns true if the move at given coordinates is valid
   */
  public boolean isValidMove(int[] coord) {
    if (coord != null) {
      if (coord[0] > 0 && coord[1] > 0 && coord[0] < gameArraySize && coord[1] < gameArraySize) {
        if (gamefield.getItemGamefield(coord[0], coord[1]) == -1) {
          return !gamefieldUtils.isSurrounded(coord[0], coord[1], playerIdx);
        } else {
          return false;
        }
      }
    }
    return false;
  }

  /**
   * Writes the move in gamefield and calls other methods to run the game
   * @param coord - coord[0] is x coordinate, coord[1] is y coordinate
   */
  public void writeMove(int[] coord) {
    gamefield.setItemGamefield(coord[0], coord[1], playerIdx);
    takeStones();
    updateScore();
    changePlayer();
    resetPass();
    if (!anyMovePoss()) {
      this.pass[playerIdx] = true;
      changePlayer();
      if (!anyMovePoss()) {
        updateWinner();
        gameRunning = false;
      }
    }
  }

  /**
   * Set pass for current player
   * Ends game if necessary
   */
  public void setPass() {
    if (pass[Math.abs(playerIdx - 1)] == true) {
      gameRunning = false;
      updateWinner();
    } else {
      this.pass[playerIdx] = true;
      changePlayer();
    }
  }

  /**
   * Ends game, sets winner
   */
  public void setSurr() {
    gameRunning = false;
    changePlayer();
    winner = playerIdx;
  }


  public boolean isGameRunning() {
    return this.gameRunning;
  }

  public int getWinner() {
    return winner;
  }

  public float[] getScore() {
    return score;
  }

  public int getPlayerIdx() {
    return playerIdx;
  }

  /**
   * Changes player (0 -> 1, 1 -> 0)
   */
  private void changePlayer() {
    playerIdx = Math.abs(playerIdx - 1);
  }

  /**
   * Calculates winner
   */
  private void updateWinner() {
    if (score[0] > score[1]) {
      winner = 0;
    } else {
      winner = 1;
    }
  }

  /**
   * @return - Returns true if there is any possible move for current player
   */
  private boolean anyMovePoss() {
    for (int y = 1; y < gameArraySize - 1; y++) {
      for (int x = 1; x < gameArraySize - 1; x++) {
        if (gamefield.getItemGamefield(x, y) == -1) {
          if (!gamefieldUtils.isSurrounded(x, y, playerIdx)) {
            return true;
          }
        }
      }
    }
    return false;
  }


  /**
   * Sets Pass to false for current player
   */
  private void resetPass() {
    this.pass[playerIdx] = false;
  }

  /**
   * Calculates score
   */
  private void updateScore() {
    score[0] = 0;
    score[1] = 0.5f;
    for (int y = 1; y < gameArraySize - 1; y++) {
      for (int x = 1; x < gameArraySize - 1; x++) {
        if (gamefield.getItemGamefield(x, y) == 0) {
          score[0] += 1;
        }
        if (gamefield.getItemGamefield(x, y) == 1) {
          score[1] += 1;
        }
      }
    }
  }

  /**
   * Checks every stone on the gamefield whether is surrounded or not
   * If there are stones which are surrounded, it removes them.
   */
  private void takeStones() {
    for (int y = 1; y < gameArraySize - 1; y++) {
      for (int x = 1; x < gameArraySize - 1; x++) {
        if (gamefieldUtils.isSurrounded(x, y)) {
          gamefieldUtils.removeStones();
        }
      }
    }
  }
}
