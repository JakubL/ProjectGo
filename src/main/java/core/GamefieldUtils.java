package core;

/**
 * Class for gamefield operations
 */
public class GamefieldUtils {
  private boolean surroundedByEnemy;
  private Gamefield gamefield;
  private int playerIdx;
  private int[][] checkArray;
  private int[][] directions = {{0, 1}, {1, 1}, {1, 0}, {1, -1}, {0, -1}, {-1, -1}, {-1, 0}, {-1, 1}};

  /**
   * Constructor takes gamefield as parameter
   * It creates an empy 2D array for checking stone position
   * @param gamefield
   */
  public GamefieldUtils(Gamefield gamefield) {
    this.gamefield = gamefield;
    checkArray = new int[gamefield.getArraySize()][gamefield.getArraySize()];
  }

  /**
   * Finds which stone is at position (x, y) and calls isSurrounded(x, y, stone)
   * @param x - Column of gamefield (1,... 8)
   * @param y - Row of gamefield (1,... 8)
   * @return - Returns true if the stone at (x, y) is surrounded by opponent's stones, otherwise returns false
   */
  public boolean isSurrounded(int x, int y) {
    int stone = gamefield.getItemGamefield(x, y);
    if (stone == 1 || stone == 0) {
      return isSurrounded(x, y, stone);
    }
    return false;
  }

  /**
   * @param x - Column of gamefield (1,... 8)
   * @param y - Row of gamefield (1,... 8)
   * @param playerIdx - Player index (0/1 - black/white)
   * @return - Returns true if the stone (= playerIdx) at (x, y) is surrounded by opponent's stones
   */
  public boolean isSurrounded(int x, int y, int playerIdx) {
    this.playerIdx = playerIdx;
    surroundedByEnemy = false;
    this.resetCheckArray();
    //surroundedByEnemy has to be true, stones can not be surrounded by the gamefield edge only
    return stoneSurrounded(x, y) && surroundedByEnemy;
  }

  /**
   * Resets a 2D array for checking stone position
   */
  private void resetCheckArray() {
    for (int i = 0; i < gamefield.getArraySize(); i++) {
      for (int j = 0; j < gamefield.getArraySize(); j++) {
        checkArray[i][j] = 0;
      }
    }
  }


  /**
   * Recursive method
   * It checks stones in every direction, if the stone is same as starting stone (= playerIdx) it calls this method again with the new stone as starting point
   * @param x - Column of gamefield (1,... 8)
   * @param y - Row of gamefield (1,... 8)
   * @return - Returns true if the stone at (x, y) is surrounded by opponent's stones, otherwise returns false
   */
  private boolean stoneSurrounded(int x, int y) {
      /* stone added to check Array */
      checkArray[x][y] = 1;
      /* check every direction */
      int dx, dy;
      for (int i = 0; i < 8; i++) {
        dx = directions[i][0];
        dy = directions[i][1];

        /* if (x + dx, y + dy) is not checked */
        if (checkArray[x + dx][y + dy] != 1) {
          /* if (x + dx, y + dy) is empty  */
          if (gamefield.getItemGamefield(x + dx, y + dy) == -1) {
            if (dirIsDiag(dx, dy)) {
              /* if last direction */
              if (i == 7) {
                int dxB = directions[i - 1][0];
                int dyB = directions[i - 1][1];
                int dxA = directions[i - 7][0];
                int dyA = directions[i - 7][1];
                /* return false if there are not opponent's stones  */
                if (!isOpponent(x + dxB, y + dyB) || !isOpponent(x + dxA, y + dyA)) {
                  return false;
                }
                /* if first direction */
              } else if (i == 0) {
                int dxB = directions[i + 7][0];
                int dyB = directions[i + 7][1];
                int dxA = directions[i - 1][0];
                int dyA = directions[i - 1][1];
                if (!(isOpponent(x + dxB, y + dyB)) || !(isOpponent(x + dxA, y + dyA))) {
                  return false;
                }
                /* other directions */
              } else {
                int dxB = directions[i - 1][0];
                int dyB = directions[i - 1][1];
                int dxA = directions[i + 1][0];
                int dyA = directions[i + 1][1];
                if (!(isOpponent(x + dxB, y + dyB)) || !(isOpponent(x + dxA, y + dyA))) {
                  return false;
                }
              }
            } else {
              return false;
            }
          }
          /* if (x + dx, y + dy) is the same stone */
          if (gamefield.getItemGamefield(x + dx, y + dy) == playerIdx) {
            if (dirIsDiag(dx, dy)) {
              if (i == 7) {
                int dxB = directions[i - 1][0];
                int dyB = directions[i - 1][1];
                int dxA = directions[i - 7][0];
                int dyA = directions[i - 7][1];
                /* If there are no opponent's stones check stoneSurrounded(x + dx, y + dy, ret) */
                if ((!isOpponent(x + dxB, y + dyB)) || (!isOpponent(x + dxA, y + dyA))) {
                  if (!stoneSurrounded(x + dx, y + dy)) {
                    return false;
                  }
                }
              } else if (i == 0) {
                int dxB = directions[i + 7][0];
                int dyB = directions[i + 7][1];
                int dxA = directions[i - 1][0];
                int dyA = directions[i - 1][1];
                if ((!isOpponent(x + dxB, y + dyB)) || (!isOpponent(x + dxA, y + dyA))) {
                  if (!stoneSurrounded(x + dx, y + dy)) {
                    return false;
                  }
                }
              } else {
                int dxB = directions[i - 1][0];
                int dyB = directions[i - 1][1];
                int dxA = directions[i + 1][0];
                int dyA = directions[i + 1][1];
                if ((!isOpponent(x + dxB, y + dyB)) || (!isOpponent(x + dxA, y + dyA))) {
                  if (!stoneSurrounded(x + dx, y + dy)) {
                    return false;
                  }
                }
              }
            } else {
              if (!stoneSurrounded(x + dx, y + dy)) {
                return false;
              }
            }
          }
          /* There must be at least one opponent's stone  */
          if (gamefield.getItemGamefield(x + dx, y + dy) == Math.abs(playerIdx - 1)) {
            surroundedByEnemy = true;
          }
        }
      }
    return true;
  }

  /**
   * Determinating whether the given direction is diagonal or not
   * directions = {{0, 1}, {1, 1}, {1, 0}, {1, -1}, {0, -1}, {-1, -1}, {-1, 0}, {-1, 1}}
   * @param dx - Horizontal parameter of direction
   * @param dy - Vertical parameter of direction
   * @return - Returns true if vector (dx, dy) is diagonal, false if not
   */
  private static boolean dirIsDiag(int dx, int dy) {
    if (dx == 0 || dy == 0) {
      return false;
    } else {
      return true;
    }
  }

  /**
   * Determinating whether the given position on gamefield belongs to opponent
   * @param x - Column of gamefield (1,... 8)
   * @param y - Row of gamefield (1,... 8)
   * @return - Returns true if there is and opponent's stone at (x, y), otherwise false
   */
  private boolean isOpponent(int x, int y) {
    if (gamefield.getItemGamefield(x, y) == Math.abs(playerIdx - 1)) {
      return true;
    }
    return false;
  }

  /**
   * Removes (sets to -1) every stone which is marked with checkArray
   */
  public void removeStones() {
    for (int y = 1; y < gamefield.getArraySize() - 1; y++) {
      for (int x = 1; x < gamefield.getArraySize() - 1; x++) {
        if (checkArray[x][y] == 1) {
          gamefield.setItemGamefield(x, y, -1);
        }
      }
    }
  }
}
