package gui;


import utils.Config;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Class which implements How to window's JPanel
 * It contains strings for describing how to play the game
 */
public class HowToPanel extends JPanel {
  private static final int WIDTH = Config.mainWIDTH;
  private static final int HEIGHT = Config.mainHEIGHT;
  private String howTo = "In every move player is able to do one thing: place stone on the desk, pass or surrender.";
  private String rules = "" +
          "The goal of the game is to have more points than the opponent at the end of the game. " +
          "The game ends when there is no possible move, both players passed or someone surrendered. " +
          "Surrendering means to automatically lose the game, even with having more points than the opponent. " +
          "Points are calculated after every move: 1 stone = 1 point for player. White has 0.5 point bonus because black plays first. " +
          "Player can place stone at any position which is not surrounded by opponent's stones. " +
          "When stones are surrounded by opponent's stones, they disappear and the positions are free again. " +
          "Stones can be surrounded even by the edge of the gamefield. " +
          "When player can not play any move, the game automatically passes." +
          "";

  public HowToPanel() {
    this.setPreferredSize(new Dimension((int) (1.2 * WIDTH), (int) (1.2 * HEIGHT)));
    this.setBackground(Color.WHITE);
    setLayout(new BorderLayout());
    JLabel textLabel = new JLabel("<html>" +
            "<h1>How to play</h1>" +
            howTo +
            "<h2>Rules</h2>" +
            rules +
            "</html>");

    textLabel.setFont(new Font("Arial", Font.BOLD, 14));
    textLabel.setHorizontalAlignment(JLabel.LEFT);
    textLabel.setVerticalAlignment(JLabel.TOP);
    setBorder(new EmptyBorder(0, 30, 0, 30));
    add(textLabel);
  }

}
