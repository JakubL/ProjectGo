# Project Go

This is a source code of a game <b>"Project Go"</b>, which is a <b>modification of a famous desk game Go</b>.

The game was developed in <b>Java Swing</b> as a <b>Maven</b> project using <b>IntelliJ IDEA</b> as IDE.

## How to install:
1) Download <br>
2) Import as Maven project (pom.xml) <br>
3) Run main.AppStart <br>

## Run compiled jar:
Requires Java <br>
path: \ProjectGo\compiledJar\ <br>
command: java -cp .\ProjectGo.jar main.AppStart <br>
Windows only: Start.bat or double click on ProjectGo.jar <br>


## Playing the game:
Players can play against a <b>very simple AI</b>, or play a game for 2 players.<br>
In every move player is able to do one thing: <b>place stone on the desk</b>, <b>pass</b> or <b>surrender</b>.<br>

### Rules:
The goal of the game is to have <b>more points</b> than the opponent at the end of the game.
<b>The game ends</b> when there is <b>no possible move</b>, <b>both players passed</b> or someone <b>surrendered</b>.
<b>Surrendering</b> means to automatically lose the game, <b>even with having more points than the opponent</b>.
Points are calculated after every move: <b>1 stone = 1 point</b> for player. White has <b>0.5 point bonus</b> because <b>black starts</b>. 
Player can place stone at any position which <b>is not surrounded by opponent's stones</b>.
When stones are surrounded by opponent's stones, <b>they disappear and the positions are free again</b>.
Stones <b>can be surrounded even by the edge of the gamefield</b>.
When player can not play any move, the game automatically passes.<br>


