package gui;

import core.Game;
import core.Gamefield;
import utils.Config;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Class which implements JPanel with multiplayer game
 */
public class PaintingPanelMulti extends JPanel {
  private Image img;
  private Game game;
  private Gamefield gamefield;
  private GraphicsPainting painting;


  /**
   * Creates gamefield, game and graphicsPainting
   */
  public PaintingPanelMulti() {
    gamefield = new Gamefield(7);
    game = new Game(gamefield);
    painting = new GraphicsPainting(gamefield);
    this.setPreferredSize(new Dimension(Config.gameWIDTH, Config.gameHEIGHT));
    this.setBackground(Color.WHITE);

    /* Image for background */
    ImageIcon imageIcon = new ImageIcon("src/main/resources/bg.jpg");
    img = imageIcon.getImage();

    MainListener mainListener = new MainListener();
    this.addMouseListener(mainListener);
  }

  public void reset(){
    gamefield = new Gamefield(7);
    game = new Game(gamefield);
    painting = new GraphicsPainting(gamefield);
    this.setPreferredSize(new Dimension(Config.gameWIDTH, Config.gameHEIGHT));
    this.setBackground(Color.WHITE);

    /* Image for background */
    ImageIcon imageIcon = new ImageIcon("src/main/resources/bg.jpg");
    img = imageIcon.getImage();

    MainListener mainListener = new MainListener();
    this.addMouseListener(mainListener);
  }

  /**
   * Implementation of paintComponent method
   * It is called with repaint() method
   *
   * @param g
   */
  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    g.drawImage(img, 0, 0, this);
    painting.drawStones(g);
    painting.drawScore(g, game.getScore());
    if (game.isGameRunning()) {
      painting.drawCurrPl(g, game.getPlayerIdx());
    } else {
      painting.drawPlayerWon(g, game.getWinner());
    }
  }

  /**
   * Conversion of point to coordinates on gamefield
   *
   * @param point point.x - x position on JPanel (int)
   *              point.y - y position on JPanel (int)
   * @return coordinates on gamefield
   * coordinates[0] - column
   * coordinates[1] - row
   */
  private int[] getMove(Point point) {
    if (point.x >= 710 || point.y >= 710 || point.x < 10 || point.y < 10) {
      return null;
    } else {
      int x = (int) ((float) (+40 + point.x) / 100 + 0.5);
      int y = (int) ((float) (+40 + point.y) / 100 + 0.5);
      return new int[]{x, y};
    }
  }

  private boolean passTriggered(Point point) {
    if (point.x >= 720 && point.y >= 435 && point.x < 905 && point.y < 515) {
      return true;
    } else {
      return false;
    }
  }

  private boolean surrTriggered(Point point) {
    if (point.x >= 720 && point.y >= 550 && point.x < 905 && point.y < 630) {
      return true;
    } else {
      return false;
    }
  }

  private class MainListener implements MouseListener {

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
    }

    /**
     * MouseListener implementation for mousePressed
     * If game is running player can:
     * 1) Surrender (game.setSurr())
     * 2) Pass (game.setPass())
     * 3) Play at specific position
     *
     * @param mouseEvent
     */
    @Override
    public void mousePressed(MouseEvent mouseEvent) {
      if (game.isGameRunning()) {
        if (surrTriggered(mouseEvent.getPoint())) {
          game.setSurr();
        } else if (passTriggered(mouseEvent.getPoint())) {
          game.setPass();
        } else {
          if (game.isValidMove(getMove(mouseEvent.getPoint()))) {
            game.writeMove(getMove(mouseEvent.getPoint()));
          } else {
          }
        }
      }
      repaint();
    }


    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
      // Do nothing
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {
      // Do nothing
    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {
      // Do nothing
    }
  }
}