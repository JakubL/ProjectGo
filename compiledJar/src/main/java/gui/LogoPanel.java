package gui;

import utils.Config;

import javax.swing.*;
import java.awt.*;

/**
 * Implementation of JPanel with logo image
 */
public class LogoPanel extends JPanel {
  ImageIcon imageIcon = new ImageIcon("src/main/resources/logo.jpg");
  Image img = imageIcon.getImage();

  public LogoPanel() {
    this.setPreferredSize(new Dimension(Config.mainWIDTH, Config.logoHEIGHT));
    this.setBackground(Color.WHITE);
    repaint();
  }

  @Override
  protected void paintComponent(Graphics graphics) {
    super.paintComponent(graphics);
    /* for some reason the panel is 10 px wider -> 5 px from left */
    graphics.drawImage(img, 5, 0, this);
  }
}
