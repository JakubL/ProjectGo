package gui;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import utils.Config;

public class MainFrame extends JFrame {
  private JPanel logoPanel;
  private JPanel buttonsPanel;
  private JFrame thisJframe = this;

  public MainFrame() {
    getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
    initComponents();
    setTitle("Project Go");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    pack();
    setVisible(true);
    setResizable(false);
    setLocationRelativeTo(null);

  }

  public void initComponents() {
    initLogoPanel();
    initButtonsPanel();


    getContentPane().add(logoPanel);
    getContentPane().add(buttonsPanel);
  }

  private void initLogoPanel() {
    logoPanel = new LogoPanel();
  }


  private void initButtonsPanel() {
    buttonsPanel = new JPanel();
    buttonsPanel.setLayout(null);
    buttonsPanel.setPreferredSize(new Dimension(Config.mainWIDTH, Config.mainHEIGHT - Config.logoHEIGHT));
    buttonsPanel.setBackground(Color.WHITE);
    JButton howToButton = new JButton("How to");
    JButton singleplayerButton = new JButton("Player vs AI");
    JButton multiplayerButton = new JButton("2 Players");
    howToButton.addActionListener(new howToButtonListener());
    singleplayerButton.addActionListener(new singleplayerButtonListener());
    multiplayerButton.addActionListener(new multiplayerButtonListener());
    int x = 80, w = 200, h = 40;
    howToButton.setBounds(x, 15, w, h);
    singleplayerButton.setBounds(x, 75, w, h);
    multiplayerButton.setBounds(x, 135, w, h);
    buttonsPanel.add(howToButton);
    buttonsPanel.add(singleplayerButton);
    buttonsPanel.add(multiplayerButton);

  }

  private void initHowToDialog() {
    JDialog howToDialog = new JDialog(this, "Project Go - How to", true);
    howToDialog.getContentPane().add(new HowToPanel(), BorderLayout.NORTH);
    howToDialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    howToDialog.setResizable(false);
    howToDialog.pack();
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Point middle = new Point(screenSize.width / 2, screenSize.height / 2);
    Point newLocation = new Point(middle.x - (howToDialog.getWidth() / 2),
        middle.y - (howToDialog.getHeight() / 2));
    howToDialog.setLocation(newLocation);
    howToDialog.setVisible(true);
  }

  private class howToButtonListener implements ActionListener {
    public void actionPerformed(ActionEvent actionEvent) {
      initHowToDialog();
    }
  }

  private class singleplayerButtonListener implements ActionListener {
    public void actionPerformed(ActionEvent actionEvent) {
      JDialog singleGame = new JDialog(thisJframe, "Project Go - Player vs AI", true);
      singleGame.getContentPane().add(new PaintingPanelSingle());
      singleGame.setResizable(false);
      singleGame.pack();
      Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
      Point middle = new Point(screenSize.width / 2, screenSize.height / 2);
      Point newLocation = new Point(middle.x - (singleGame.getWidth() / 2), middle.y - (singleGame.getHeight() / 2));
      singleGame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
      singleGame.setLocation(newLocation);
      singleGame.setVisible(true);
    }
  }

  private class multiplayerButtonListener implements ActionListener {
    public void actionPerformed(ActionEvent actionEvent) {
      JDialog multiGame = new JDialog(thisJframe, "Project Go - 1 vs 1", true);
      multiGame.getContentPane().add(new PaintingPanelMulti());
      multiGame.setResizable(false);
      multiGame.pack();
      Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
      Point middle = new Point(screenSize.width / 2, screenSize.height / 2);
      Point newLocation = new Point(middle.x - (multiGame.getWidth() / 2), middle.y - (multiGame.getHeight() / 2));
      multiGame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
      multiGame.setLocation(newLocation);
      multiGame.setVisible(true);
    }
  }

}
