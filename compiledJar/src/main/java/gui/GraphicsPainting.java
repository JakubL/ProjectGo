package gui;

import core.Gamefield;
import utils.Config;

import java.awt.*;
import java.text.DecimalFormat;

/**
 * Class for drawing on JPanel
 * As a parameter it gets a gamefield
 */
public class GraphicsPainting {
  private Gamefield gamefield;

  public GraphicsPainting(Gamefield gamefield) {
    this.gamefield = gamefield;
  }

  /**
   * Draws stones on Graphics (JPanel)
   *
   * @param g
   */
  public void drawStones(Graphics g) {
    for (int y = 1; y < gamefield.getArraySize() - 1; y++) {
      for (int x = 1; x < gamefield.getArraySize() - 1; x++) {
        if (gamefield.getItemGamefield(x, y) == 0) {
          drawStoneAt(g, x, y, 0);
        }
        if (gamefield.getItemGamefield(x, y) == 1) {
          drawStoneAt(g, x, y, 1);
        }
      }
    }
  }

  /**
   * Draws stone at specific position
   *
   * @param g - Graphics object
   * @param x - Column of gamefield
   * @param y - Row of gamefield
   * @param stone - Player index (black/white)
   */
  private void drawStoneAt(Graphics g, int x, int y, int stone) {
    int coordX = -40 + x * 100, coordY = -40 + y * 100;
    if (stone == 0) {
      g.setColor(Color.BLACK);
      g.fillOval(coordX - Config.stoneWIDTH / 2, coordY - Config.stoneWIDTH / 2, Config.stoneWIDTH, Config.stoneWIDTH);
    }
    if (stone == 1) {
      g.setColor(Color.WHITE);
      g.fillOval(coordX - Config.stoneWIDTH / 2, coordY - Config.stoneWIDTH / 2, Config.stoneWIDTH, Config.stoneWIDTH);
    }
  }

  /**
   * Draws stone of the current player at the specific position
   *
   * @param g
   * @param player - Player index (0/1 - black/white)
   */
  public void drawCurrPl(Graphics g, int player) {
    if (player == 0) {
      g.setColor(Color.BLACK);
    } else {
      g.setColor(Color.WHITE);
    }
    g.fillOval(810, 110, 50, 50);
  }


  /**
   * Draws score (players stone + score)
   *
   * @param g
   * @param score
   * score[0] - score of player with index 0
   * score[1] - score of player with index 1
   */
  public void drawScore(Graphics g, float[] score) {
    Font currentFont = g.getFont();
    Font newFont = currentFont.deriveFont(30f);
    g.setFont(newFont);
    g.setColor(Color.BLACK);
    g.fillOval(750, 190, 40, 40);
    DecimalFormat df = new DecimalFormat();
    df.setMaximumFractionDigits(0);
    g.drawString(df.format(score[0]), 800, 220);
    g.setColor(Color.WHITE);
    g.fillOval(750, 240, 40, 40);
    df.setMaximumFractionDigits(1);
    g.drawString(df.format(score[1]), 800, 270);
  }

  /**
   * Draws stone of the winner at the specific position
   *
   * @param g
   * @param winner - Player index (0/1 - black/white)
   */
  public void drawPlayerWon(Graphics g, int winner) {
    if (winner == 0) {
      g.setColor(Color.BLACK);
    } else {
      g.setColor(Color.WHITE);
    }
    g.fillOval(790, 330, 70, 70);

  }

}
